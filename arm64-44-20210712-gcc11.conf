[bat]
BAT_BISECTION_OLD=v4.4.270
BAT_BISECTION_NEW=v4.4.275
BAT_GIT_DIR=/data/linux-stable-rc

[build]
echo "***** BUILD *****"
# Build kernel using Tuxbuild
tuxsuite build --git-repo https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git \
  --target-arch arm64 \
  --toolchain gcc-11 \
  --json-out build.json \
  --kconfig defconfig \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/lkft.config  \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/lkft-crypto.config  \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/distro-overrides.config  \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/systemd.config  \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/virtio.config  \
  --kconfig CONFIG_ARM64_MODULE_PLTS=y \
  --kconfig CONFIG_SYN_COOKIES=y \
  --git-sha ${BAT_KERNEL_SHA}

build_ret=$?

export build_json="$(readlink -e build.json)"

GIT_DESCRIBE="$(jq -r .git_describe "${build_json}")"
DOWNLOAD_URL="$(jq -r .download_url "${build_json}")"
# The URL ends with /, so remove the last one
DOWNLOAD_URL="$(echo "${DOWNLOAD_URL}" | cut -d/ -f1-4)"
KERNEL_NAME=$(curl -sSL ${DOWNLOAD_URL}/metadata.json|jq -r '.results.artifacts.kernel[0]')
MODULES_NAME=$(curl -sSL ${DOWNLOAD_URL}/metadata.json|jq -r '.results.artifacts.modules[0]')

export BAT_LAVA_REFERENCE=3053797
export BAT_LAVA_JOBNAME="lkft-bisection ltp qemu-arm64 fs bug dd ${GIT_DESCRIBE}"
export BAT_PUB_KERNEL="${DOWNLOAD_URL}/${KERNEL_NAME}"
export BAT_PUB_MODULES="${DOWNLOAD_URL}/${MODULES_NAME}"
export BAT_PUB_MODULES_COMPRESSION="xz"
export BAT_PUB_ROOTFS="https://storage.lkft.org/rootfs/oe-sumo/20210525/juno/rpb-console-image-lkft-juno-20210525221209.rootfs.ext4.gz"
export BAT_PUB_ROOTFS_FORMAT="ext4"
export BAT_PUB_ROOTFS_COMPRESSION="gz"


# This is the same for all lava jobs
####################################
[test]
echo "***** TEST *****"
# Write out LAVA job
rm -f job.yaml res.json
curl -sSL -o lava-job-reproduce.sh https://gitlab.com/Linaro/lkft/lkft-bisection/-/raw/master/lava-job-reproduce.sh
bash -x ./lava-job-reproduce.sh -b "${build_json}" -l "${BAT_LAVA_REFERENCE}" > job.yaml

LAVAJOB=$(lavacli jobs submit job.yaml)
LAVA_LOG="log-${BAT_KERNEL_SHA_SHORT}"
lavacli jobs show ${LAVAJOB}
set +e
while true; do
  lavacli jobs wait "${LAVAJOB}"
  if [ $? -eq 0 ]; then break; fi
done
set -e
lavacli jobs logs ${LAVAJOB} > ${LAVA_LOG}
lavacli results --json ${LAVAJOB} > results.json
####################################

[discriminator]
# Look for specific message in the log:
res="$(jq -r '.[] | select(.metadata.case=="0_prep-tmp-disk") | .metadata.result' results.json)"
if [ "${res}" = "fail" ]; then
  echo " ****************************************************** "
  echo " JOB FAILED!"
  echo " ****************************************************** "
  echo " See ${LAVA_LOG}".
  bat_new
else
  echo " ****************************************************** "
  echo " All fine and dandy."
  echo " ****************************************************** "
  echo " See ${LAVA_LOG}".
  bat_old
fi
