#!/bin/bash

set -e

show_help() {
  echo "$0 -b build.json_file -l LAVA_job_ID"
}

# Find out build.json format
# $*: Name of the build.json file
find_build_json_root() {
  build_json="$*"
  if [ ! -f "${build_json}" ]; then return; fi

  if ! jq keys "${build_json}" | grep -q git_describe > /dev/null; then
    json_root=".[]"

    len="$(jq length "${build_json}")"
    if [ "${len}" -gt 1 ]; then
      echo "WARNING: More than one build in ${build_json}. Using data from first build." >&2
      json_root=".[0]"
    fi
  fi

  echo -n "${json_root}"
}

# Defaults
BUILD_JSON="build.json"
LAVA_JOB_ID=""

# Override defaults with arguments:
while getopts "b:hl:" arg; do
  case ${arg} in
    b)
      BUILD_JSON="${OPTARG}"
      ;;
    h)
      show_help
      exit 0
      ;;
    l)
      LAVA_JOB_ID="${OPTARG}"
      ;;
    *)
      show_help
      exit 1
      ;;
  esac
done
shift $((OPTIND - 1))

if [ -z "${LAVA_JOB_ID}" ]; then
  echo "No LAVA job ID specified"
  show_help
  exit 1
fi

if [ ! -e "${BUILD_JSON}" ]; then
  echo "File ${BUILD_JSON} not found"
  show_help
  exit 1
fi

job_definition="$(mktemp)"
new_job_definition="$(mktemp)"
metadata_json="$(mktemp)"

# shellcheck disable=SC2086
lavacli ${LAVA_OPTS} jobs definition "${LAVA_JOB_ID}" > "${job_definition}"

device_type="$(yq -r .device_type "${job_definition}")"

json_root=$(find_build_json_root "${BUILD_JSON}")
git_describe="$(jq -r "${json_root}.git_describe" "${BUILD_JSON}")"
download_url="$(jq -r "${json_root}.download_url" "${BUILD_JSON}")"
# The URL ends with /, so remove the last one
download_url="${download_url%*/}"

curl -sSL "${download_url}/metadata.json" > "${metadata_json}"
kernel_name="$(jq -r '.results.artifacts.kernel | .[0]' "${metadata_json}")"
modules_name="$(jq -r '.results.artifacts.modules | .[0]' "${metadata_json}")"

# Retrieve information from LAVA job definition
job_name="$(yq -r '.job_name' "${job_definition}")"
case "${device_type}" in
  bcm2711-rpi-4-b)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.modules.url' "${job_definition}" | head -n1)"
    dtb_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.dtb.url' "${job_definition}" | head -n1)"
    dtb_name="dtbs/${dtb_url#*/dtbs/}"
    ;;
  dragonboard-410c)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.modules.url' "${job_definition}" | head -n1)"
    dtb_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.dtb.url' "${job_definition}" | head -n1)"
    dtb_name="dtbs/${dtb_url#*/dtbs/}"
    ;;
  dragonboard-845c)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.modules.url' "${job_definition}" | head -n1)"
    dtb_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.dtb.url' "${job_definition}" | head -n1)"
    dtb_name="dtbs/${dtb_url#*/dtbs/}"
    ;;
  hi6220-hikey-r2)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.modules.url' "${job_definition}" | head -n1)"
    dtb_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.dtb.url' "${job_definition}" | head -n1)"
    dtb_name="dtbs/${dtb_url#*/dtbs/}"
    ;;
  juno-r2)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.modules.url' "${job_definition}" | head -n1)"
    dtb_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.dtb.url' "${job_definition}" | head -n1)"
    dtb_name="dtbs/${dtb_url#*/dtbs/}"
    ;;
  nxp-ls2088)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.modules.url' "${job_definition}" | head -n1)"
    dtb_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.dtb.url' "${job_definition}" | head -n1)"
    dtb_name="dtbs/${dtb_url#*/dtbs/}"
    # echo kernel_url="yq -r '.actions[] | select(has("deploy")) | .deploy.kernel.url' "${job_definition}" | head -n1"
    # echo "kernel url: [${kernel_url}]"
    # exit 0
    ;;
  x15)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.modules.url' "${job_definition}" | head -n1)"
    dtb_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.dtb.url' "${job_definition}" | head -n1)"
    dtb_name="dtbs/${dtb_url#*/dtbs/}"
    ;;
  x86)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.modules.url' "${job_definition}" | head -n1)"
    ;;
  qemu)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.rootfs.overlays.modules.url' "${job_definition}" | head -n1)"
    ;;
  qemu-arm)
    kernel_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.kernel.url' "${job_definition}" | head -n1)"
    modules_url="$(yq -r '.actions[] | select(has("deploy")) | .deploy.images.rootfs.overlays.modules.url' "${job_definition}" | head -n1)"
    ;;
  *)
    echo "Unknown device: ${device_type}"
    exit 1
    ;;
esac

# Apply changes onto new LAVA job definition
cp -p "${job_definition}" "${new_job_definition}"
sed -i -e "s#${kernel_url}#${download_url}/${kernel_name}#g" "${new_job_definition}"
sed -i -e "s#${modules_url}#${download_url}/${modules_name}#g" "${new_job_definition}"
sed -i -e "s#${job_name}#deep-bisection ${git_describe}#" "${new_job_definition}"
sed -i -e "s#^priority: .*#priority: 81#" "${new_job_definition}"

case "${device_type}" in
  dragonboard-410c | dragonboard-845c | hi6220-hikey-r2 | juno-r2 | nxp-ls2088 | x15)
    sed -i -e "s#${dtb_url}#${download_url}/${dtb_name}#g" "${new_job_definition}"
    ;;
esac

#echo "---v---diff---v---"
#diff -Naur "${job_definition}" "${new_job_definition}" ||:
#echo "---^----------^---"

cat "${new_job_definition}"
#rm "${job_definition}" "${new_job_definition}" "${metadata_json}"
rm "${new_job_definition}" "${metadata_json}"
